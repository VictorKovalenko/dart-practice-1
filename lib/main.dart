import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(),
      //
    );
  }
}

// ignore: must_be_immutable
class MyHomePage extends StatelessWidget {
  int value = 10;

  List<int> list = [];

  void multiplication() {
    for (int x = 0; x <= value; x++) {
      for (int y = 0; y <= value; y++) {
        if (x == 0) {
          list.add(x);
        } else if (y == 0) {
          list.add(y);
        } else {
          list.add(x * y);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    int numberX = 0;
    int numberY = 0;

    multiplication();

    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Container(
        color: Colors.black26,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount:11,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
          itemCount: 121,
          itemBuilder: (BuildContext ctx, index) {
            return list[index] == 0
                ? Container(
                    alignment: Alignment.center,
                    child: Text(
                      '${numberX != 11 ? numberX++ : numberY += 1}',
                      style: TextStyle(color: Colors.black),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.green[400],
                    ),
                  )
                : Container(
                    alignment: Alignment.center,
                    child: Text(
                      '${list[index]}',
                      style: TextStyle(color: Colors.black),
                    ),
                    decoration: BoxDecoration(
                      color:index%12==0? Colors.yellow:index%12 > index%11? Colors.orange[300]:Colors.white,

                    ),
                  );
          },
        ),
      ),
    );
  }
}


